from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import render, redirect
from www import forms, models


# Create your views here.
def index(request, form=forms.StatusForm()):
    context = {
            'form': form,
            'statuses': models.Status.objects.all(),
        }
    return render(request, 'index.html', context)


def status(request, status_id, form=forms.StatusForm()):
    if request.method == 'POST':
        reply_form = forms.StatusForm(request.POST)
        if reply_form.is_valid():
            reply = reply_form.save(commit=False)
            reply.parent = models.Status.objects.get(id=status_id)

            reply.save()

        else:
            return status(request, status_id, form=reply_form)

    context = {
        'form': form,
        'status': models.Status.objects.get(id=status_id),
    }

    return render(request, 'status.html', context)


def submit(request):
    if request.method == 'POST':
        status = forms.StatusForm(request.POST)
        if status.is_valid():
            status.save()
            return redirect('/')

        else:
            return index(request, form=status)

    else:
        return redirect('/')


def profile(request):
    return render(request, 'profile.html')


def botm(request):
    return render(request, 'botm.html')


def botm_api(request):
    import requests
    botm_json = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()

    return JsonResponse(botm_json)


def subscribe(request, form=forms.SubscriberForm()):
    if request.method == 'POST':
        import json
        data = json.loads(request.body)
        subscriber = forms.SubscriberForm(data)
        if subscriber.is_valid():
            with transaction.atomic():
                subscriber.save()
            return JsonResponse({"message": "Success!"})
        else:
            return JsonResponse(subscriber.errors, status=400)

    context = {'form': form}
    return render(request, 'subscribe.html', context)


def subscribe_check(request, email, name, password):
    form = forms.SubscriberForm(
            {'email': email, 'name': name, 'password': password})
    if form.is_valid():
        return JsonResponse({"message": "OK"})
    else:
        return JsonResponse(form.errors, status=400)


def get_subscribers(request):
    subs = list(models.Subscriber.objects.values())
    return JsonResponse({"data": subs})


def del_subscriber(request):
    if request.method == 'POST':
        import json
        email = json.loads(request.body)
        sub = models.Subscriber.objects.get(**email)
        sub.delete()
        return JsonResponse({"message": "OK"})
    return JsonResponse({"message": "No data to return."})

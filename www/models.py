from django.db import models


# Create your models here.
class Status(models.Model):
    parent = models.ForeignKey('self', on_delete=models.CASCADE,
                               null=True, related_name='replies')
    status = models.CharField(max_length=140)
    created_at = models.DateTimeField(auto_now_add=True)


class Subscriber(models.Model):
    email = models.EmailField(unique=True)
    name = models.TextField()
    password = models.CharField(max_length=32)

from django.urls import path, re_path
from www import views

urlpatterns = [
        re_path(r'^$', views.index, name='index'),
        path('status/<int:status_id>', views.status, name='status'),
        re_path(r'^submit$', views.submit, name='submit'),
        re_path(r'^profile$', views.profile, name='profile'),
        re_path(r'^botm$', views.botm, name='botm'),
        re_path(r'^botm-api$', views.botm_api, name='botm_api'),
        re_path(r'^subscribe$', views.subscribe, name='subscribe'),
        path('subscribe/check/<str:email>/<str:name>/<slug:password>',
             views.subscribe_check, name='subscribe_check'),
        path('get-subscribers', views.get_subscribers),
        path('subscriber/delete', views.del_subscriber),
    ]

from django import forms
from www.models import Status, Subscriber


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        error_messages = {
                'status': {
                    'required': "Status shouldn't be left blank.",
                    'max_length': 'Status should be 140 characters or less.',
                }
            }
        widgets = {
                'status': forms.Textarea(attrs={
                    'class': 'form-control',
                    'rows': 4,
                    'cols': 100,
                })
            }


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = 'email', 'name', 'password'

        widgets = {
                'email': forms.EmailInput(attrs={
                    'class': 'form-control',
                    'title': 'Enter a valid email address.',
                }),
                'name': forms.TextInput(attrs={
                    'class': 'form-control',
                    'title': 'Name field cant be empty.'}),
                'password': forms.PasswordInput(attrs={
                    'class': 'form-control',
                    'pattern': '[a-zA-Z0-9_-]{8,32}',
                    'title': '8 to 32 Alphanumerics combined with dash or underscore.'}),
            }

        # No validation method for regex or minimum length of name
        # Django will only check for model unique=True
        # The rest of the validation done in client

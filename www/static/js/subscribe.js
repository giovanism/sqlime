// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

var disable = function() {
  $("button").prop("disabled", true);
  document.getElementById("submit_button").classList.add("btn-default");
  document.getElementById("submit_button").classList.add("btn-ghost");
  document.getElementById("warning").classList.remove("alert-success");
  document.getElementById("warning").classList.add("alert-warning");
}

var enable = function() {
  $("button").prop("disabled", false);
  document.getElementById("submit_button").classList.remove("btn-default");
  document.getElementById("submit_button").classList.remove("btn-ghost");
  document.getElementById("warning").classList.remove("alert-warning");
  document.getElementById("warning").classList.add("alert-success");
  $("#warning").html("Form is valid.");
}

var subscribe = function() {
  var email = $("#id_email").val();
  var name = $("#id_name").val();
  var passwd = $("#id_password").val();
  var csrftoken = $("input[name=csrfmiddlewaretoken]").val();
  $.ajax({
    url: "/subscribe",
    method: "POST",
    dataType: "json",
    headers: {"X-CSRFToken": csrftoken},
    data: `{"email": "${email}", "name": "${name}", "password": "${passwd}"}`,
    success: function (response) {
      console.log(response);
      $("#warning").html("Success. Entry is saved.");
    },
    statusCode: {
      400: function (response) { console.log(response); }
    },
  });
}

$("#subscribe_form input").on("input", function() {
  var email = $("#id_email");
  var name = $("#id_name");
  var passwd = $("#id_password");
  if (!email.is(":invalid") && !name.is(":invalid") && !passwd.is(":invalid"))
    $.ajax({
      url: `/subscribe/check/${email.val()}/${name.val()}/${passwd.val()}`,
      success: function (response) {
        enable();
      },
      statusCode: {
        400: function (response) {
          var errors = response.responseJSON;
          console.log(errors);
          disable();
          var warning = "";
          for (var error in errors) {
            warning += `<p>${errors[error].join("<br />")}</p>`;
          };
          $("#warning").html(warning);
        }
      }
    });
  else {
    disable();
    var warning = "";
    $("#subscribe_form input:invalid").each((i, invalid) => warning += `<p>${invalid.title}</p>` );
    $("#warning").html(warning);
  }
});

var subscribers = [];

var refresh = function () {
  $.ajax({
    url: "/get-subscribers",
    success: function (response) {
      subscribers = response.data;
      console.table(subscribers);

      var subs = "";
      subscribers.forEach(sub => {
        subs += `<li>
                   <div>
                     <h5>${sub.email}</h5>
                     <button onclick="unsubscribe('${sub.email}')" class="btn btn-error">Unsubscribe</button>
                   </div>
                 </li>`;
      });
      console.log(subs);

      $("#sub_list").html(subs);
    },
  });
}

var unsubscribe = function (email) {
  var sub = subscribers.find(sub => sub.email == email);
  var passwd = prompt("Please fill the corresponding password", "You can hack around this obviously LOL");
  if (passwd == sub.password) {
    var csrftoken = $("input[name=csrfmiddlewaretoken]").val();
    $.ajax({
      url: "/subscriber/delete",
      method: "POST",
      headers: {"X-CSRFToken": csrftoken},
      dataType: "json",
      data: `{"email": "${email}"}`,
      success: function (response) {
        console.log("Deleted: " + email);
        refresh();
      },
    });
  } else {
    alert("Password salah");
  }
}

refresh();

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

favorites = [];

var updateCounter = function () {
  $("#counter").html(favorites.length);
};

var favor = function(id) {
  console.log(id);
  var i = favorites.indexOf(id);
  var new_icon = "star";
  if (i != -1) {
    favorites.remove(i);
    new_icon = "star_border";
  } else {
    favorites.push(id);
  }
  $(`#${id} i`).html(new_icon);
  updateCounter();
};

$.ajax({
    url: "/botm-api",
    success: function (response) {
      var items = response.items;
      items.forEach(function (item) {
        var prevContent = $("tbody").html();
        $("tbody").html(
          `${prevContent}
           <tr id="${item.id}">
             <td><img src="${item.volumeInfo.imageLinks.smallThumbnail}" /></td>
             <td>
               <a target="_blank" href="${item.volumeInfo.previewLink}">
                 ${item.volumeInfo.title}
               </a>
             </td>
             <td>${item.volumeInfo.categories.join(", ")}</td>
             <td>${
                 item.volumeInfo.industryIdentifiers
                     .map(isbn => isbn.type + ": " + isbn.identifier)
                     .join("<br />")
                   }</td>
             <td>
               <button onclick="favor('${item.id}')" class="btn btn-warning btn-ghost">
                 <i class="material-icons">star_border</i>
               </button>
             </td>
           </tr>
           `);
      });
    },
}).done(function () {
  $(".loading").hide();
});

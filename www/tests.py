from django.test import TestCase
from django.utils import timezone
from www import views, models, forms


# Create your tests here.
class Story6TestCase(TestCase):
    def test_landing_page_routing(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.index)

    def test_landing_page_contains_hello_apa_kabar(self):
        response = self.client.get('/')
        self.assertContains(response, 'Hello apa kabar?')

    def test_landing_page_templates(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_can_create_a_new_status(self):
        models.Status.objects.create(
            status='Returns OK 200',
            created_at=timezone.now()
        )
        status_count = models.Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_form_status_element_has_attributes(self):
        form_as_p = forms.StatusForm().as_p()
        self.assertIn('class="form-control"', form_as_p)
        self.assertIn('id="id_status"', form_as_p)

    def test_form_validation_for_blank_status(self):
        form = forms.StatusForm(data={
            'status': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'], ["Status shouldn't be left blank."]
        )

    def test_form_validation_for_exceeding_status_length(self):
        form = forms.StatusForm(data={
            'status': 'generated_pseudo_random_bytes' * 100,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ['Status should be 140 characters or less.']
        )

    def test_form_post_action_routing(self):
        response = self.client.post('/submit')
        self.assertEqual(response.resolver_match.func, views.submit)

    def test_get_from_submit_view(self):
        response = self.client.get('/submit')
        self.assertRedirects(response, '/')

    def test_form_post_object_creation(self):
        example_status = "Returns OK 200"
        response = self.client.post('/submit',
                {
                    'status': example_status,
                }
            )
        last_status = models.Status.objects.last()
        self.assertEqual(last_status.status, example_status)

    def test_status_rendered(self):
        example_status = "Returns OK 200"
        models.Status.objects.create(
            status=example_status,
        )
        response = self.client.get('/')
        self.assertContains(response, example_status)

    def test_status_rendered_on_post(self):
        example_status = "Returns OK 200"
        response = self.client.post('/submit',
                {
                    'status': example_status,
                }
            )
        self.assertRedirects(response, '/')
        response = self.client.get('/')
        self.assertContains(response, example_status)


class Story6ChallengeTestCase(TestCase):
    def test_profile_page_routing(self):
        response = self.client.get('/profile')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.profile)

    def test_profile_page_contains_my_profile(self):
        response = self.client.get('/profile')
        self.assertContains(response, 'Giovan Isa Musthofa')
        self.assertContains(response, '1706040126')
        self.assertContains(response, 'Faculty of Computer Science')


class Story9TestCase(TestCase):
    def test_books_of_the_month_page_routing(self):
        response = self.client.get('/botm')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.botm)

    def test_books_of_the_month_page_template(self):
        response = self.client.get('/botm')
        self.assertTemplateUsed(response, 'botm.html')

    def test_botm_api_routing(self):
        response = self.client.get('/botm-api')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.botm_api)

    def test_botm_api(self):
        response = self.client.get('/botm-api')
        self.assertEquals(response['Content-Type'], 'application/json')


class Story10TestCase(TestCase):
    def test_subscribe_page_routing(self):
        response = self.client.get('/subscribe')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.subscribe)

    def test_subscribe_page_template(self):
        response = self.client.get('/subscribe')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_can_register_a_new_subscriber(self):
        models.Subscriber.objects.create(
                email='me@me.me',
                name='Alex',
                password='GodOnlyKnows')
        subscribers_count = models.Subscriber.objects.all().count()
        self.assertEquals(subscribers_count, 1)

    def test_form_subscriber_element_has_attributes(self):
        form_as_p = forms.SubscriberForm().as_p()
        self.assertIn('class="form-control"', form_as_p)
        self.assertIn('id="id_email', form_as_p)
        self.assertIn('id="id_name', form_as_p)
        self.assertIn('id="id_password', form_as_p)

    def test_form_validation_for_invalid_email(self):
        form = forms.SubscriberForm(data={
            'email': 'username.at.gmail.com',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'], ['Enter a valid email address.']
        )


class Story10ChallengeTestCase(TestCase):
    def test_increase_coverage_0(self):
        self.client.get('/subscribe')

    def test_increase_coverage_1(self):
        self.client.get('/subscriber/delete')

    def test_increase_coverage_2(self):
        self.client.get('/get-subscribers')

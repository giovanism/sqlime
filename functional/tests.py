from django.test import LiveServerTestCase
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options


class Story7TestCase(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        opts = Options()
        args = ('--dns-prefetch-disable', '--no-sandbox',
                '--headless', '--disable-gpu')
        for arg in args:
            opts.add_argument(arg)
        cls.selenium = Chrome(chrome_options=opts)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_hello_apa_kabar(self):
        self.selenium.get(self.live_server_url)
        title = self.selenium.find_element_by_tag_name('h1')
        self.assertTrue(title.is_displayed())
        self.assertIn('Hello apa kabar?', title.text)

    def test_hello_apa_kabar_style(self):
        self.selenium.get(self.live_server_url)
        title = self.selenium.find_element_by_tag_name('h1')
        font_family = title.value_of_css_property('font-family')
        self.assertIn('monospace', font_family)
        color = title.value_of_css_property('color')
        self.assertEquals('rgba(204, 204, 204, 1)', color)

    def test_status_form(self):
        self.selenium.get(self.live_server_url)
        status_form = self.selenium.find_element_by_id('status_form')
        textarea = status_form.find_element_by_tag_name('textarea')
        submit_button = status_form.find_element_by_tag_name('button')

        self.assertTrue(status_form.is_displayed())
        self.assertTrue(textarea.is_displayed())
        self.assertTrue(submit_button.is_displayed())

    def test_status_form_style(self):
        self.selenium.get(self.live_server_url)
        status_form = self.selenium.find_element_by_id('status_form')
        textarea = status_form.find_element_by_tag_name('textarea')
        submit_button = status_form.find_element_by_tag_name('button')

        self.assertEquals(status_form.get_property('action'), f'{self.live_server_url}/submit')

        self.assertEquals(textarea.get_property('id'), 'id_status')
        self.assertEquals(textarea.get_attribute('maxlength'), '140')

        self.assertEquals(submit_button.text, 'Submit')
        self.assertEquals(submit_button.value_of_css_property('color'), 'rgba(255, 255, 255, 1)')
        self.assertEquals(submit_button.value_of_css_property('background-color'), 'rgba(33, 150, 243, 1)')
        self.assertEquals(submit_button.get_property('form'), status_form)

    def test_submit_status(self):
        self.selenium.get(self.live_server_url)
        textarea = self.selenium.find_element_by_id('id_status')
        status_sample = 'Returns OK 200'
        textarea.send_keys(status_sample)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.assertIn(status_sample, self.selenium.page_source)

    def test_profile_page_routing(self):
        self.selenium.get(f'{self.live_server_url}/profile')
        self.assertIn('Giovan Isa Musthofa', self.selenium.page_source)
        self.assertIn('Faculty of Computer Science', self.selenium.page_source)
        self.assertIn('1706040126', self.selenium.page_source)
